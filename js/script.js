/**
 * Author: Kovács Lajos (@diwin)
 * Email: kovacs.lajos1218@gmail.com
 * Date: 2016.06.17
 */



var ngDragnDrop = angular.module("ngDragnDrop", ['ngDraggable']);

// TODO: This factory only needs when we have a backend with an api.
/*
We can use $http() too instead of resource.
 */
/*ngDragnDrop.factory("cartDb", function ($resource) {
    return $resource('/api/1/cart/', {userid: '@id'}, {
        update: {
            method: 'PUT'
        }
    });
});*/

//TODO: Add cartDb to the function argument list and to the injector.
ngDragnDrop.controller("dndCtrl",
    ['$scope',
        function($scope) {

            $scope.currentUser = {
                userid: 0
            };
            
            $scope.availableItems = [
                {"id":1, "name": 'Iphone 5s', "price": 150000, "thumbnail": 'img/iphone.jpg'},
                {"id":2, "name": 'Whirlpool top-freeze refrigerator', "price": 350000, "thumbnail": 'img/fridge.png'},
                {"id":3, "name": 'Nexus Player', "price": 30000, "thumbnail": 'img/nexus.jpg'}
            ];
            // $scope.cartItems = cartDb.get({userid: $scope.currentUser.userid});  TODO: If we have a backend and an api for the cart, than uncomment this line.

            $scope.cartItems = [];

            $scope.saveCart = function() {
                /*
                * We have to add the userid and the cartItems to the resource update, because we have to pass the current user id to the api and some data what we want to manipulate on the server!
                */
                var save = {
                    userid: $scope.currentUser.userid,
                    cartItems: $scope.cartItems
                };

                // cartDb.update(save); TODO: If we have a backend and an api for the cart, than uncomment this line.
                console.log("PUT request sent!");
            };
            
            $scope.onDropComplete=function(data,event){
                if(data.quantity == undefined) {
                    var search = {};
                    for (var i = 0; i < $scope.cartItems.length; i++) {
                        search[$scope.cartItems[i].id] = $scope.cartItems[i];
                    }
                    if(search[data.id] == undefined)
                    {
                        var pushableData = {
                            "id":data.id,
                            "name": data.name,
                            "price": data.price,
                            "thumbnail":data.thumbnail,
                            "quantity":1
                        };
                        $scope.cartItems.push(pushableData);
                        $scope.saveCart();

                    } else {
                        for (var i = 0, len = $scope.cartItems.length; i < len; i++) {
                            if($scope.cartItems[i].id == data.id) {
                                $scope.cartItems[i].quantity += 1;
                            }
                        }
                        $scope.saveCart();
                    }
                }
            };
            $scope.onFilterComplete=function(index,data,event){
                if(data.quantity != undefined) {
                    var otherObj = $scope.cartItems[index];
                    var otherIndex = $scope.cartItems.indexOf(data);
                    $scope.cartItems[index] = data;
                    $scope.cartItems[otherIndex] = otherObj;
                    $scope.saveCart();
                }
            };
        }
    ]
);


